# Lancement local

Suivi du readme du repository de base, il faut se placer dans chaque projet avant de lancer la commande '../mvnw spring-boot:run'
Important : les projets config-server et discovery-server sont à lancer avant les autres pour que tout fonctionne

# Lancement Docker

Suivi du readme du repository de base, exécution de la commande './mvnw clean install -PbuildDocker' puis 'docker-compose up'
Ceci lance tous les services du projet
